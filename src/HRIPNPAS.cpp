#include <boost/thread/thread.hpp>

#include <actionlib/server/simple_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <boost/algorithm/string.hpp>
#include <tcp_interface/RCOMMessage.h>
#include <laser_analysis/LaserObstacleMap.h>

#include "topics.h"

#include "HRIPNPAS.h"

HRIPNPActionServer::HRIPNPActionServer(ros::NodeHandle n) : PNPActionServer(), handle(n), handlep("~")  
{
    event_pub = handle.advertise<std_msgs::String>(TOPIC_PNPCONDITION, 10);
    plantoexec_pub = handle.advertise<std_msgs::String>(TOPIC_PLANTOEXEC, 10);
    rcom_pub= handle.advertise<tcp_interface::RCOMMessage>(TOPIC_RCOMMESSAGE,10);

    register_action("noaction",&PNPActionServer::none,(PNPActionServer*)this);
    register_action("waitfor",&PNPActionServer::waitfor,(PNPActionServer*)this);
    register_action("restart",&PNPActionServer::restartcurrentplan,(PNPActionServer*)this);

    register_action("turn",&HRIPNPActionServer::turn,this);
    register_action("goto",&HRIPNPActionServer::followcorridor,this);
    register_action("gotoOffice",&HRIPNPActionServer::goto_office,this);
    register_action("move",&HRIPNPActionServer::goto_movebase,this);
    register_action("say",&HRIPNPActionServer::say,this);
    register_action("beep",&HRIPNPActionServer::beep,this);
    register_action("ask",&HRIPNPActionServer::ask,this);
    register_action("askimg",&HRIPNPActionServer::askimg,this);
    register_action("display",&HRIPNPActionServer::display,this);
    register_action("followcorridor",&HRIPNPActionServer::followcorridor,this);
    register_action("followperson",&HRIPNPActionServer::followperson,this);
    register_MRaction("arrive",&HRIPNPActionServer::arrive,this);

    register_action("takephoto",&HRIPNPActionServer::takephoto,this);
    register_action("choose",&HRIPNPActionServer::choose,this);

    register_action("enableMove",&HRIPNPActionServer::enableMove,this);
    register_action("disableMove",&HRIPNPActionServer::disableMove,this);
    register_action("fixedMove",&HRIPNPActionServer::fixedMove,this);
    register_action("enableLegTracker",&HRIPNPActionServer::enableLegTracker,this);
    register_action("disableLegTracker",&HRIPNPActionServer::disableLegTracker,this);


    register_action("Bye",&HRIPNPActionServer::bye,this);
    register_action("Ask",&HRIPNPActionServer::ask,this);

    register_MRaction("Goto",&HRIPNPActionServer::MRfollowcorridor,this);
    register_MRaction("Follow",&HRIPNPActionServer::MRfollowperson,this);
    register_MRaction("Answer",&HRIPNPActionServer::answer,this);
    register_MRaction("Prepare",&HRIPNPActionServer::prepare,this);
    register_MRaction("Take",&HRIPNPActionServer::take,this);
//    register_MRaction("Prepares",&HRIPNPActionServer::prepare,this);
//    register_MRaction("Poses",&HRIPNPActionServer::take,this);

    handle.param("robot_name",robotname,string("diago"));

    listener = new tf::TransformListener();

    ac_movebase = NULL; ac_turn = NULL; ac_followcorridor = NULL; ac_followperson = NULL;

    
    laser_obsmap_sub = handle.subscribe(TOPIC_LASER_OBSMAP, 1, &HRIPNPActionServer::laserobsmapCallback, this);
    tcp_sub = handle.subscribe(TOPIC_RCOMMESSAGE, 10, &HRIPNPActionServer::tcpCallback, this);
    //cond_sub = handle.subscribe(TOPIC_PNPCONDITION, 10, &HRIPNPActionServer::conditionCallback, this);
    active_places_sub = handle.subscribe(TOPIC_PNPACTIVEPLACES, 10, &HRIPNPActionServer::active_places_callback, this);

    PNP_cond_pub = handle.advertise<std_msgs::String>(TOPIC_PNPCONDITION, 10);

    //last_condition_received="";

    targetGX=-999; targetGY=-999; targetGTh_deg=999;

}


void HRIPNPActionServer::active_places_callback(const std_msgs::String::ConstPtr& msg)
{
    ConditionCache.clear();
    if (msg->data=="init;") {
        do_display("init");
        ROS_INFO("Init place -> initialize GUI");
    }
}

