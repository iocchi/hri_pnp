#include <boost/thread/mutex.hpp>
#include <tf/transform_listener.h>
#include <tcp_interface/RCOMMessage.h>
#include <boost/algorithm/string.hpp>

#include "HRIPNPAS.h"
#include "topics.h"

#define RAD(a) ((a)/180.0*M_PI)
#define DEG(a) ((a)*180.0/M_PI)

#define SPEECH_TIMEOUT 120 // sec (used also for display)


using namespace std;

bool HRIPNPActionServer::getRobotPose(std::string robotname, double &x, double &y, double &th_rad) {
    if (listener==NULL) {
        listener = new tf::TransformListener();
    }

    string src_frame = "/" + robotname + "/map";
    string dest_frame = "/" + robotname + "/base_frame";
    if (robotname=="") { // local trasnformation
        src_frame = "map";
        dest_frame = "base_link";
    }

    tf::StampedTransform transform;
    try {
        listener->waitForTransform(src_frame, dest_frame, ros::Time(0), ros::Duration(3));
        listener->lookupTransform(src_frame, dest_frame, ros::Time(0), transform);
    }
    catch(tf::TransformException ex) {
        th_rad = 999999;
        ROS_ERROR("Error in tf trasnform %s -> %s\n",src_frame.c_str(), dest_frame.c_str());
        ROS_ERROR("%s", ex.what());
        return false;
    }
    x = transform.getOrigin().x();
    y = transform.getOrigin().y();
    th_rad = tf::getYaw(transform.getRotation());

    return true;
}


bool HRIPNPActionServer::getLocationPosition(string loc, double &GX, double &GY) {


    if (loc=="home") {
        GX = 12.0; GY = 2.0;
    }
    else if (loc=="printer") {
        GX = 2.0; GY = 22.0;
    }
    else if (loc=="toilet") {
        GX = 26.0; GY = 2.0;
    }
    else if (loc=="entrance") {
        GX = 12.0; GY = 2.0;
    }
    else if (loc=="target") {
        GX = 2.0; GY = 22.0;
    }
    else if (loc=="RoomB109") {
        GX = 2.0; GY = 6.0;
    }
    else if (loc=="RoomB110") {
        GX = 2.0; GY = 6.0;
    }
    else if (loc=="RoomB115") {
        GX = 10.0; GY = 1.8;
    }
    else if (loc=="RoomB117") {
        GX = 14.0; GY = 2.0;
    }
    else if (loc=="diagentrance") {
        GX = 12.5; GY = 13.0;
    }
    else if (loc=="diagcenterlobby") {
        GX = 12.5; GY = 20.0;
    }
    else if (loc=="pecciolientrance") {
        GX = 7.5; GY = 6.5;
    }
    else if (loc=="peccioliexit") {
        GX = 10.0; GY = 4.0;
    }
    else if (loc=="pecciolic1U") {
        GX = 6.5; GY = 17.5;
    }
    else if (loc=="pecciolic1D") {
        GX = 6.0; GY = 8.0;
    }
    else if (loc=="pecciolic2U") {
        GX = 8.0; GY = 17.5;
    }
    else if (loc=="pecciolibedroomin") {
        GX = 3.0; GY = 18.0;
    }
    else if (loc=="pecciolibedroomout") {
        GX = 5.7; GY = 17.5;
    }
    else if (loc=="pecciolilivingroom") {
        GX = 3.5; GY = 3.5;
    }
    else if (loc=="pecciolikitchen") {
        GX = 8.0; GY = 11.0;
    }
    else if (loc=="pkitchentable") {
        GX = 16.7; GY = 15.0;
    }
    else if (loc=="pbedroom") {
        GX = 10.6; GY = 7.9;
    }
    else if (loc=="plivingroom") {
        GX = 24.3; GY = 9.8;
    }
    else if (loc=="pfridge") {
        GX = 12.7; GY = 13.5;
    }
    else if (loc=="pexit") {
        GX = 25.0; GY = 18.0;
    }
    else if (loc=="phall") {
        GX = 21.0; GY = 13.0;
    }
    else if (loc=="leonlivingroom") {
        GX = 10.9; GY = 10.1;
    }
    else if (loc=="leonbathroom") {
        GX = 14.0; GY = 12.4;
    }
    else if (loc=="leonbedroom") {
        GX = 16.3; GY = 15.15;
    }    
    else if (loc=="leonkitchen") {
        GX = 13.1; GY = 15.7;
    }    
    else if (loc=="leonentrance") {
        GX = 10.1; GY = 18.3;
    }
    else if (loc=="leontechnicalroom") {
        GX = 12.0; GY = 22.0;
    }

    else if (loc=="leonwp1") {
        GX = 10.9; GY = 10.1;
    }
    else if (loc=="leonwp2") {
        GX = 14.0; GY = 12.4;
    }
    else if (loc=="leonwp3") {
        GX = 16.3; GY = 15.15;
    }    
    else if (loc=="leonwp4") {
        GX = 10.1; GY = 18.3;
    }    
    else if (loc=="leonwp5") {
        GX = 10.3; GY = 15.1;
    }
    else if (loc=="leonwp6") {
        GX = 10.7; GY = 12.7;
    }
    else if (loc=="leonwp7") {
        GX = 14; GY = 10.3;
    }
    else if (loc=="leonwp8") {
        GX = 15.8; GY = 10.3;
    }
    else if (loc=="leonwp9") {
        GX = 16.57; GY = 13.3;
    }
    else if (loc=="leonwp10") {
        GX = 13.15; GY = 15.8;
    }
    else {
        ROS_ERROR_STREAM("Location "<<loc<<" unknown.");
        return false;
    }

    ROS_INFO_STREAM("Location " << loc << " at " << GX  << " , " << GY);  
    
    return true;
}

bool HRIPNPActionServer::getOfficePosition(string topic, string &room) {


    if (topic=="ai") {
        room = "RoomB117";
    }
    else if (topic=="ml") {
        room = "RoomB109";
    }
    else if (topic=="cv") {
        room = "RoomB110";
    }
    else if (topic=="pr") {
        room = "RoomB115";
    }
    else if (topic=="hri") {
        room = "RoomB115";
    }
    else {
        ROS_ERROR_STREAM("Office for "<< topic <<" unknown.");
        return false;
    }

    ROS_INFO_STREAM("Office for " << topic << " is " << room);  
    
    return true;
}




/*
 * ACTIONS
 */


void HRIPNPActionServer::MRfollowcorridor(string agent, string params, bool *run) {

    boost::to_lower(agent);
    if (agent=="robot" || agent==robotname) {
       followcorridor(params, run);
    }
    else {
        ROS_INFO_STREAM(agent << " is executing " << params);

        // HUMAN GOTO_TARGET

        ros::Duration s(1);
        s.sleep();

    }
}

void HRIPNPActionServer::followcorridor(string params, bool *run) {

  if (!run) return;

  cout << "### Executing Follow Corridor " << params << " ... " << endl;

  double GX,GY, RX,RY,RTH;
  if (!getRobotPose(robotname, RX, RY, RTH)) {
    ROS_ERROR("Follow Corridor: Cannot determine robot pose.");
    return;
  }

  if (!getLocationPosition(params,GX,GY)) {
    ROS_ERROR("Follow Corridor: Cannot find location %s.",params.c_str());
    return;
  }

  targetGX=GX; targetGY=GY; targetGTh_deg=999;

  if (*run)
    do_follow_corridor(GX,GY,run);

  targetGX=-999; targetGY=-999; targetGTh_deg=999;

  cout << "### Follow Corridor " << params << ((*run)?" Completed":" Aborted") << endl;

}


void HRIPNPActionServer::MRfollowperson(string agent, string params, bool *run) {

    if (agent=="ROBOT" || agent=="Robot" || agent==robotname) {
       followperson(params, run);
    }
    else {
        ROS_INFO_STREAM(agent << " is executing " << params);

        ros::Duration s(1);
        s.sleep();

    }
}

void HRIPNPActionServer::followperson(string params, bool *run) {

  if (!run) return;

  cout << "### Executing Follow Person " << params << " ... " << endl;

  do_follow_person(run);

  cout << "### Follow Person " << params << ((*run)?" Completed":" Aborted") << endl;

}


void HRIPNPActionServer::turn(string params, bool *run) {

  if (!run) return;

  cout << "### Executing Turn " << params << " ... " << endl;

  float th_deg = 0;
  string absrel_flag = "REL";

  vector<string> vparams;
  boost::split(vparams, params, boost::is_any_of("_")); // split parameters
  if (vparams.size()==1) {
    th_deg = atof(vparams[0].c_str());
  }
  else {
    absrel_flag=vparams[0];
    th_deg = atof(vparams[1].c_str());
  }

  do_turn(absrel_flag,th_deg,run);

  cout << "### Turn " << params << ((*run)?" Completed":" Aborted") << endl;
}


void HRIPNPActionServer::goto_movebase(string params, bool *run) {

  cout << "### Executing Move " << params << " ... " << endl;

  double GX,GY;
  if (getLocationPosition(params,GX,GY)) {
    do_movebase(GX,GY,0,run);
  }
  else 
    ROS_WARN("Move: Cannot find location %s.",params.c_str());

  cout << "### Move " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::goto_office(string params, bool *run) {

  cout << "### Executing goto_office " << params << " ... " << endl;

  string room;
  if (getOfficePosition(params,room)) {
      followcorridor(room,run);
  }
  else 
    ROS_WARN("Move: Cannot find office %s.",params.c_str());

  cout << "### goto_office " << params << ((*run)?" Completed":" Aborted") << endl;

}


void HRIPNPActionServer::bye(string params, bool *run) {
    display("text_goodbye",run);
}

void HRIPNPActionServer::say(string params, bool *run) {
  cout << "### Executing Say " << params << " ... " << endl;

  if (!*run)
      return;

  string to_send = "say_" + params;
  tcp_interface::RCOMMessage message_to_send;
  message_to_send.robotsender= robotname;
  message_to_send.robotreceiver="all";
  message_to_send.value= to_send;
  rcom_pub.publish(message_to_send);
  
  end_speech=false;
  int sleeptime=SPEECH_TIMEOUT;
  bool psim=false;
  ros::param::get("/use_sim_time",psim);
	cout << "DEBUG:: say " << psim << endl;
  if (psim) sleeptime=3;
  while (*run && sleeptime-->0 && !end_speech && ros::ok())
    ros::Duration(1.0).sleep();
  end_speech=false;

  cout << "### Say " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::beep(string params, bool *run) {
  cout << "### Executing Beep " << params << " ... " << endl;

  if (!*run)
      return;

  internal_clear_buffer();

  string to_send = "beep";
  tcp_interface::RCOMMessage message_to_send;
  message_to_send.robotsender= robotname;
  message_to_send.robotreceiver="all";
  message_to_send.value= to_send;
  rcom_pub.publish(message_to_send);
  
  cout << "### Beep " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::ask(string params, bool *run) {
  cout << "### Executing Ask " << params << " ... " << endl;

  // waitfor("personhere",run);

  string to_send = "ask_" + params;
  tcp_interface::RCOMMessage message_to_send;
  message_to_send.robotsender= robotname;
  message_to_send.robotreceiver="all";
  message_to_send.value= to_send;
  rcom_pub.publish(message_to_send);

  end_speech=false;
  int sleeptime=SPEECH_TIMEOUT;
  while (*run && sleeptime-->0 && !end_speech && ros::ok())
    ros::Duration(1.0).sleep();
  end_speech=false;

  cout << "### Ask " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::askimg(string params, bool *run) {
  cout << "### Executing AskImg " << params << " ... " << endl;

  // waitfor("personhere",run);

  string to_send = "askimg_" + params;
  tcp_interface::RCOMMessage message_to_send;
  message_to_send.robotsender= robotname;
  message_to_send.robotreceiver="all";
  message_to_send.value= to_send;
  rcom_pub.publish(message_to_send);

  end_speech=false;
  int sleeptime=SPEECH_TIMEOUT;
  while (*run && sleeptime-->0 && !end_speech && ros::ok())
    ros::Duration(1.0).sleep();
  end_speech=false;

  cout << "### AskImg " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::do_display(string params) {

    // send through tcp_interface a string "display_{text|image|video}_arg"

    string to_send = "display_" + params;
    tcp_interface::RCOMMessage message_to_send;
    message_to_send.robotsender= robotname;
    message_to_send.robotreceiver="all";
    message_to_send.value= to_send;
    rcom_pub.publish(message_to_send);
}

void HRIPNPActionServer::display(string params, bool *run) {

  cout << "### Executing Display " << params << " ... " << endl;
  

  // non-terminating action
  //while (*run && ros::ok())
  //  ros::Duration(0.5).sleep();


  do_display(params);
  //say(params, run);

  if (params.substr(0,4)=="text"||params.substr(0,6)=="txtimg") {
      end_speech=false;
      int sleeptime=SPEECH_TIMEOUT;
      while (*run && sleeptime-->0 && !end_speech && ros::ok())
        ros::Duration(1.0).sleep();
      end_speech=false;
    }

  cout << "### Display " << params << ((*run)?" Completed":" Aborted") << endl;

}


void HRIPNPActionServer::takephoto(string params, bool *run) {

    cout << "### Executing Takephoto " << params << " ... " << endl;

    tcp_interface::RCOMMessage message_to_send;
    message_to_send.robotsender= robotname;
    message_to_send.robotreceiver="all";
    message_to_send.value="TAKE PIC *"+params;

    rcom_pub.publish(message_to_send);
    ros::Duration s(1);
    s.sleep();

    cout << "### Takephoto " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::arrive(string agent, string params, bool *run) {
    
    cout << "### Executing by agent: " << agent << " action Arrive " << params << " ... " << endl;

    waitfor("personhere",run);
    
    cout << "### " << agent << " Arrive " << params << ((*run)?" Completed":" Aborted") << endl;    
    
}


void HRIPNPActionServer::answer(string agent, string params, bool *run) {

    cout << "### Executing by agent: " << agent << " action Answer " << params << " ... " << endl;

    waitfor("HRIreceived",run);

    cout << "### " << agent << " Answer " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::prepare(string agent, string params, bool *run) {

    cout << "### Executing by agent: " << agent << " action Prepare " << params << " ... " << endl;

    ask("putobject",run);
    //say("OK. Please put the object on my tray and tell me when you have done.",run);

    waitfor("done",run);

    /*
    if (params=="SmallObject") {
        ros::Duration s(1); s.sleep();
        say("Let's go",run);
    }
    else {
        ros::Duration s(5); s.sleep();
        
    }*/

    cout << "### " << agent << " Prepare " << params << ((*run)?" Completed":" Aborted") << endl;
}


void HRIPNPActionServer::take(string agent, string params, bool *run) {

    cout << "### Executing by agent: " << agent << " action Take " << params << " ... " << endl;

    waitfor("personhere",run);

    ask("takeobject",run);
    // display("Please take the object from my tray.",run);

    ros::Duration s(1); s.sleep();
    // waitfor("HRIreceived",run);

    waitfor("done",run);

    cout << "### " << agent << " Take " << params << ((*run)?" Completed":" Aborted") << endl;
}


void HRIPNPActionServer::choose(string params, bool *run) {

    cout << "### Executing action Choose " << params << " ... " << endl;

    string outcond = params;

    char sr[20];
    if (params=="joke") {
        int k= (rand()%14)+1;
        sprintf(sr,"%03d",k);
    }
    else if (params=="quiz") {
        int k= (rand()%16);
        sprintf(sr,"%03d",k);
    }
    else if (params=="category") {
        int k= (rand()%2);
        sprintf(sr,"%s",(k==0)?"animal":"country");
    }
    else if (params=="animal") {
        int k= (rand()%22);
        sprintf(sr,"%03d",k);
        outcond="element";
    }
    else if (params=="country") {
        int k= (rand()%19);
        sprintf(sr,"%03d",k);
        outcond="element";
    }


    std_msgs::String out;
    out.data = outcond+"_"+string(sr);
    cout << "Random choice: " << out.data << endl;
    PNP_cond_pub.publish(out);
    ros::Duration s(0.5);
    s.sleep();

    cout << "### Choose " << params << ((*run)?" Completed":" Aborted") << endl;
}






void HRIPNPActionServer::do_movebase(float GX, float GY, float GTh_DEG, bool *run) { // theta in degrees

  targetGX=GX; targetGY=GY; targetGTh_deg=GTh_DEG;

  mtx_movebase.lock();

/*
  std::string def_force_scale, def_momentum_scale;

  ros::param::get("/"+robotname+"/gradientBasedNavigation/force_scale", def_force_scale);
  ros::param::get("/"+robotname+"/gradientBasedNavigation/momentum_scale", def_momentum_scale);

  ros::param::set("/"+robotname+"/gradientBasedNavigation/force_scale", 0.4);
  ros::param::set("/"+robotname+"/gradientBasedNavigation/momentum_scale", 0.3);
*/

  double force_scale = 0.4, momentum_scale = 0.3;

  handle.setParam("/"+robotname+"/gradientBasedNavigation/force_scale", force_scale);
  handle.setParam("/"+robotname+"/gradientBasedNavigation/momentum_scale", momentum_scale);
  ros::spinOnce();


  if (ac_movebase==NULL) { //create the client only once
    // Define the action client (true: we want to spin a thread)
    ac_movebase = new actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>(TOPIC_MOVE_BASE, true);

    // Wait for the action server to come up
    while(!ac_movebase->waitForServer(ros::Duration(5.0))){
	    ROS_INFO("Waiting for move_base action server to come up");
    }
  }

  // Read time
  double secs =ros::Time::now().toSec();
  while (secs==0) {  // NEEDED OTHERWISE CLOCK WILL BE 0 AND GOAL_ID IS NOT SET CORRECTLY
	  ROS_ERROR_STREAM("Time is null: " << ros::Time::now());
	  ros::Duration(0.25).sleep();
      secs =ros::Time::now().toSec();
  }

  // Set the goal (MAP frame)
  move_base_msgs::MoveBaseGoal goal;

  goal.target_pose.header.frame_id = "/" + robotname + "/map";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = GX;
  goal.target_pose.pose.position.y = GY;
  goal.target_pose.pose.orientation.z = sin(RAD(GTh_DEG)/2);
  goal.target_pose.pose.orientation.w = cos(RAD(GTh_DEG)/2);

  // Send the goal
  ROS_INFO("move_base: sending goal %.1f %.1f",GX,GY);
  ac_movebase->sendGoal(goal);

  // Wait for termination (check distance every delay seconds)
  double delay = 0.1;
  double d_threshold=0.5, d=d_threshold+1.0;
  while (!ac_movebase->waitForResult(ros::Duration(delay)) && (*run) && (d>d_threshold)) {
    // ROS_INFO("Running...");
    double RX,RY,RTH;
    if (getRobotPose(robotname, RX, RY, RTH))
      d = fabs(GX-RX)+fabs(GY-RY);
  }

  // Cancel all goals (NEEDED TO ISSUE NEW GOALS LATER)
  ac_movebase->cancelAllGoals(); ros::Duration(0.1).sleep(); // wait a little

  mtx_movebase.unlock();

}



void HRIPNPActionServer::do_turn(string absrel_flag, float GTh_DEG, bool *run) {

    if (ac_turn==NULL) {
      ac_turn = new actionlib::SimpleActionClient<rococo_navigation::TurnAction>(TOPIC_TURN, true);

      while(!ac_turn->waitForServer(ros::Duration(5.0))){
              ROS_INFO("Waiting for turn action server to come up");
      }
    }

    rococo_navigation::TurnGoal goal;

    goal.target_angle = GTh_DEG;
    goal.absolute_relative_flag = absrel_flag;
    goal.max_ang_vel = 50.0;  // deg/s

    // Send the goal
    ROS_INFO("Sending goal TURN %f", GTh_DEG);
    ac_turn->cancelAllGoals(); ros::Duration(0.1).sleep();
    ac_turn->sendGoal(goal);

    while (!ac_turn->waitForResult(ros::Duration(0.1)) && (*run)){
       // ROS_INFO("Turning...");
    }
    ac_turn->cancelAllGoals();
}



void HRIPNPActionServer::do_follow_corridor(float GX, float GY, bool *run) {

  double force_scale = 0.6, momentum_scale = 0.2;

  handle.setParam("/"+robotname+"/gradientBasedNavigation/force_scale", force_scale);
  handle.setParam("/"+robotname+"/gradientBasedNavigation/momentum_scale", momentum_scale);
  ros::spinOnce();

  cout << "Follow corridor params - gbn: " <<  force_scale << " " << momentum_scale << endl;
/*
  double par;
  handle.getParam("/"+robotname+"/gradientBasedNavigation/force_scale",par);

  if (par<0.001)
    ROS_ERROR("force_scale gbn too low!!!");
  else
    ROS_INFO("force_scale gbn: %.2f", par);
*/

  if (ac_followcorridor==NULL) {
    // Define the action client (true: we want to spin a thread)
    ac_followcorridor = new actionlib::SimpleActionClient<rococo_navigation::FollowCorridorAction>(TOPIC_FOLLOWCORRIDOR, true);

    // Wait for the action server to come up
    while(!ac_followcorridor->waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for follow_corridor action server to come up");
    }
  }

  // Read time
  double secs =ros::Time::now().toSec();
  while (secs==0) {  // NEEDED OTHERWISE CLOCK WILL BE 0 AND GOAL_ID IS NOT SET CORRECTLY
      ROS_ERROR_STREAM("Time is null: " << ros::Time::now());
      ros::Duration(0.25).sleep();
    secs =ros::Time::now().toSec();
  }

  // Set the goal (MAP frame)
  rococo_navigation::FollowCorridorGoal goal;
  goal.target_X = GX;  goal.target_Y = GY;   // goal
  goal.max_vel = 1.0;  // m/s

  // Send the goal
  ROS_INFO("Sending goal follow corridor %.1f %.1f",GX,GY);
  ac_followcorridor->sendGoal(goal);

  // Wait for termination
  double d_threshold=0.5, d=d_threshold+1.0;
  while (!ac_followcorridor->waitForResult(ros::Duration(0.1)) && (*run) && (d>d_threshold)) {
      // ROS_INFO("Running...");
      double RX,RY,RTH;
      if (getRobotPose(robotname, RX, RY, RTH))
        d = fabs(GX-RX)+fabs(GY-RY);
      if (d<d_threshold*2.0)
        handle.setParam("/"+robotname+"/gradientBasedNavigation/force_scale", force_scale*0.66);
  }

  handle.setParam("/"+robotname+"/gradientBasedNavigation/force_scale", force_scale);

  // Cancel all goals (NEEDED TO ISSUE NEW GOALS LATER)
  ac_followcorridor->cancelAllGoals(); ros::Duration(0.1).sleep(); // wait 
}


void HRIPNPActionServer::do_follow_person(bool *run) {

    double max_vel = 1.0; 

    if (ac_followperson==NULL) {
        ac_followperson = new actionlib::SimpleActionClient<rococo_navigation::FollowPersonAction>(TOPIC_FOLLOWPERSON, true);
    }

    // Wait for the action server to come up
    while(!ac_followperson->waitForServer(ros::Duration(5.0))){
        ROS_INFO("Waiting for follow_person action server to come up");
    }

    // Cancel all goals (JUST IN CASE SOME GOAL WAS NOT CLOSED BEFORE)
    ac_followperson->cancelAllGoals(); ros::Duration(0.1).sleep();

    // Set the goal
    rococo_navigation::FollowPersonGoal goal;
    goal.person_id = 0;      // unused so far
    goal.max_vel = max_vel;  // m/s

    // Send the goal
    ROS_INFO("Sending goal");
    ac_followperson->sendGoal(goal);

	  // Wait for termination
    while (!ac_followperson->waitForResult(ros::Duration(0.1)) && (*run)) {
	    // ROS_INFO_STREAM("Running... [" << ac_followperson.getState().toString() << "]");
    }
    // ROS_INFO_STREAM("Finished [" << ac.getState().toString() << "]");

    // Print result
    if (ac_followperson->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        ROS_INFO("FollowPerson successful");
    else
        ROS_INFO("FollowPerson failed");

    // Cancel all goals (NEEDED TO ISSUE NEW GOALS LATER)
    ac_followperson->cancelAllGoals(); ros::Duration(0.1).sleep(); // wait 

}


void HRIPNPActionServer::fixedMove(string params, bool *run){
  cout << "### Executing Fixed Move " << params << " ... " << endl;
  double GX, GY, RX, RY, RTH, IRX, IRY, IRTH;
  if (!getRobotPose(robotname, IRX, IRY, IRTH)) {
    ROS_ERROR("Fixed move: Cannot determine robot pose.");
    return;
  }

  ros::Publisher desired_cmd_vel_pub = handle.advertise<geometry_msgs::Twist>("/"+robotname+"/desired_cmd_vel", 1);

  double force_scale_old, momentum_scale_old;
  handle.getParam("/"+robotname+"/gradientBasedNavigation/force_scale", force_scale_old);
  handle.setParam("/"+robotname+"/gradientBasedNavigation/force_scale", 0.05);
  handle.getParam("/"+robotname+"/gradientBasedNavigation/momentum_scale", momentum_scale_old);
  handle.setParam("/"+robotname+"/gradientBasedNavigation/momentum_scale", 0.0);


  //Desired translation in x
  double tx;// = atof(params.c_str());
  double timeout_factor;

  vector<string> vparams;
  boost::split(vparams, params, boost::is_any_of("_")); // split parameters
  if (vparams.size()==1) {
    tx = atof(vparams[0].c_str());
    timeout_factor = 20;
  }
  else {
    tx = atof(vparams[0].c_str());
    timeout_factor = atof(vparams[1].c_str());
  }

  //Desired goal
  GX = RX + tx * cos(RTH);
  GY = RY + tx * sin(RTH);

  RX = IRX;
  RY = IRY;
  double threshold = 0.05;
  double velocity = 0.4;
  double timeout = (tx / velocity)*timeout_factor;
  double rate = 0.1;
  double timer = 0.0;
  while (fabs(IRX-RX)<tx && fabs(IRY-RY)<tx && timer < timeout ){
    geometry_msgs::Twist cmd;
    cmd.linear.x = velocity;
    desired_cmd_vel_pub.publish(cmd);
    ros::Duration(rate).sleep(); // wait ...    
    getRobotPose(robotname, RX, RY, RTH);
    timer += rate;
  }

  handle.setParam("/"+robotname+"/gradientBasedNavigation/force_scale", force_scale_old); //Restoring
  handle.setParam("/"+robotname+"/gradientBasedNavigation/momentum_scale", momentum_scale_old);

  //stop robot
  geometry_msgs::Twist cmd;
  cmd.linear.x = 0;
  desired_cmd_vel_pub.publish(cmd);
  ros::Duration(rate).sleep(); // wait ...    
  
}

void HRIPNPActionServer::enableLegTracker(string params, bool *run) {
    cout << "### Executing EnableParam " << params << " ... " << endl;

    //Boolean
    handle.setParam("/diago/leg_tracker/enabled", true);
}

void HRIPNPActionServer::disableLegTracker(string params, bool *run) {
    cout << "### Executing DisableParam " << params << " ... " << endl;

    //Boolean
    handle.setParam("/diago/leg_tracker/enabled", false);
}

void HRIPNPActionServer::enableMove(string params, bool *run) {
    cout << "### Executing EnableMove " << params << " ... " << endl;

    tcp_interface::RCOMMessage message_to_send;
    message_to_send.robotsender= robotname;
    message_to_send.robotreceiver="192.168.1.2";
    message_to_send.value="enable";

    rcom_pub.publish(message_to_send);
    ros::Duration s(1);
    s.sleep();

    cout << "### EnableMove " << params << ((*run)?" Completed":" Aborted") << endl;

}

void HRIPNPActionServer::disableMove(string params, bool *run) {
    cout << "### Executing DisableMove " << params << " ... " << endl;

    tcp_interface::RCOMMessage message_to_send;
    message_to_send.robotsender= robotname;
    message_to_send.robotreceiver="192.168.1.2";
    message_to_send.value="disable";

    rcom_pub.publish(message_to_send);
    ros::Duration s(1);
    s.sleep();

    cout << "### DisableMove " << params << ((*run)?" Completed":" Aborted") << endl;

}
