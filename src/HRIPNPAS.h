#ifndef __HRIPNPAS_H__
#define __HRIPNPAS_H__

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <tf/transform_listener.h>
#include <std_msgs/String.h>

#include <pnp_ros/PNPActionServer.h>
#include <rococo_navigation/TurnAction.h>
#include <rococo_navigation/FollowCorridorAction.h>
#include <rococo_navigation/FollowPersonAction.h>

#include <tcp_interface/RCOMMessage.h>
#include <laser_analysis/LaserObstacleMap.h>

#include <map>
#include <boost/thread/thread.hpp>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;


class HRIPNPActionServer : public PNPActionServer
{
private:
    ros::NodeHandle handle, handlep;
    ros::Publisher event_pub, plantoexec_pub, hri_pub, rcom_pub;
    tf::TransformListener* listener;

    // action clients
    actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> *ac_movebase;
    actionlib::SimpleActionClient<rococo_navigation::TurnAction> *ac_turn;
    actionlib::SimpleActionClient<rococo_navigation::FollowCorridorAction> *ac_followcorridor;
    actionlib::SimpleActionClient<rococo_navigation::FollowPersonAction> *ac_followperson;

    // condition subscribers
    ros::Subscriber laser_obsmap_sub; // receiving data from laser_analysis
    ros::Subscriber tcp_sub; // receiving data from tcp_interface
    ros::Subscriber cond_sub; // receiving data from PNP condition
    ros::Subscriber active_places_sub; // receiving labels of active places in PNP

    ros::Publisher PNP_cond_pub;
    
    std::string robotname;
    boost::mutex mtx_movebase;

    double targetGX, targetGY, targetGTh_deg; // current goal target set by goto action

public:

    HRIPNPActionServer(ros::NodeHandle n);

    // Get current robot pose
    bool getRobotPose(string robotname, double &x, double &y, double &th_rad);

    // Get coordinates of semantic location
    bool getLocationPosition(string loc, double &GX, double &GY);

    // Get name of room for topic
    bool getOfficePosition(string topic, string &room);
    
    /*
     * ACTIONS
     */
    void advertise(string params, bool *run);
    void advertiseComplex(string params, bool *run);
    void interact(string params, bool *run);
    void swipe(string params, bool *run);
    void goto_movebase(string params, bool *run);
    void goto_office(string params, bool *run);
    void turn(string params, bool *run);
    void followcorridor(string params, bool *run);
    void followperson(string params, bool *run);
    void say(string params, bool *run);
    void beep(string params, bool *run);
    void bye(string params, bool *run);
    void ask(string params, bool *run);
    void askimg(string params, bool *run);
    void display(string params, bool *run);
    void takephoto(string params, bool *run);

    void arrive(string agent, string params, bool *run);
    void answer(string agent, string params, bool *run);
    void MRfollowcorridor(string agent, string params, bool *run);
    void MRfollowperson(string agent, string params, bool *run);
    void prepare(string agent, string params, bool *run);
    void take(string agent, string params, bool *run);

    void choose(string params, bool *run);

    void do_movebase(float GX, float GY, float GTh_DEG, bool *run);
    void do_turn(string absrel_flag, float GTh_DEG, bool *run);
    void do_follow_corridor(float GX, float GY, bool *run);
    void do_follow_person(bool *run);
    void do_display(string params);

    void enableMove(string params, bool *run);
    void disableMove(string params, bool *run);
    void fixedMove(string params, bool *run);

    void enableLegTracker(string params, bool *run);
    void disableLegTracker(string params, bool *run);

    /*
     * CONDITIONS FUNCTIONS AND CALLBACKS
     */
    
    virtual int evalCondition(string cond);
//    void conditionCallback(std_msgs::String msg);

    void laserobsmapCallback(laser_analysis::LaserObstacleMap msg);
    void tcpCallback(tcp_interface::RCOMMessage msg);
    void active_places_callback(const std_msgs::String::ConstPtr& msg);
    
//    string last_condition_received;
    bool end_speech;

};

#endif

