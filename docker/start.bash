#!/bin/bash

composefile=""
if [ "$1" == "-dev" ]; then
  composefile="-f docker-compose-dev.yml"
fi

SESSION=diagdemo

tmux -2 new-session -d -s $SESSION

tmux rename-window -t $SESSION:0 'compose'
tmux new-window -t $SESSION:1 -n 'stage'
tmux new-window -t $SESSION:2 -n 'loc'
tmux new-window -t $SESSION:3 -n 'nav'
tmux new-window -t $SESSION:4 -n 'rviz'
tmux new-window -t $SESSION:5 -n 'quit'

tmux send-keys -t $SESSION:0 "docker-compose $composefile up" C-m
sleep 5

tmux send-keys -t $SESSION:1 "echo 'DISB1' | netcat -w 1 localhost 9235" C-m

#docker exec -it stage bash -ci \"rosrun stage_environments start_simulation.py DISB1\"" C-m
sleep 3

tmux send-keys -t $SESSION:2 "docker exec -it rchomeedu bash -ci \"cd ~/src/marrtino_apps/navigation && xterm -e 'python startloc.py'\"" C-m
sleep 3

tmux send-keys -t $SESSION:3 "docker exec -it rchomeedu bash -ci \"cd ~/src/marrtino_apps/navigation && xterm -e 'roslaunch move_base.launch'\"" C-m
sleep 3

tmux send-keys -t $SESSION:4 "docker exec -it rchomeedu bash -ci \"cd ~/src/marrtino_apps/navigation && rosrun rviz rviz -d nav.rviz\""

#docker exec -it rchomeedu bash -ci "rosnode list"

docker exec -it rchomeedu tmux

#docker exec -it rchomeedu bash -ci "cd ~/src/DIAG_demo/actions/goto && python goto_actionproxy.py printer1"



