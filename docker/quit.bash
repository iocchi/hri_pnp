#!/bin/bash

SESSION=diagdemo

tmux send-keys -t $SESSION:5 "echo '@killstage' | netcat -w 1 localhost 9235" C-m
sleep 2
tmux send-keys -t $SESSION:5 "echo '@quitserver' | netcat -w 1 localhost 9235" C-m
sleep 2

tmux send-keys -t $SESSION:3 C-c
sleep 2

tmux send-keys -t $SESSION:2 C-c
sleep 2

tmux send-keys -t $SESSION:0 C-c
sleep 2
tmux send-keys -t $SESSION:0 C-c
sleep 2

tmux kill-session -t $SESSION

