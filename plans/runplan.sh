#!/bin/sh
# Use: ./runplan.sh <robotname> <planname>
# Example: ./runplan.sh diago_0 test

./genplan.sh $2.plan wpnav.er
rostopic pub /$1/planToExec std_msgs/String "data: '$2'" --once


