#!/bin/sh
# Use: ./pnpcond.sh <robotname> <condition>
# Example: ./pnpcond.sh diago_0 test

rostopic pub /$1/PNPConditionEvent std_msgs/String "data: '$2'" --once

