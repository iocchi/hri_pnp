#!/usr/bin/env python

import Tkinter as tk
import tkMessageBox
import ConfigParser
import thread
from Tkinter import *
from ttk import *
import Image, tkFileDialog
import numpy as np
import sys, time, os, glob, shutil, math, datetime



Robots = [ 'diago_0' ]

Plans = ['stop', 'sample1']

Conditions = ['personAt_RoomB110']





##############
#    GUI
##############

class DIP(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent) 
        self.parent = parent
        self.initUI()

	
    def initUI(self):
        self.parent.title("PNP Manager")
        self.style = Style()
        self.style.theme_use("alt")
        self.parent.resizable(width=FALSE, height=FALSE)
        self.pack(fill=BOTH, expand=1)
                
        self.robot_ddm = StringVar(self)
        self.plan_ddm = StringVar(self)
        self.cond_ddm = StringVar(self)


        _row = 0

        # Robot
        lbl = Label(self, text="Robot")
        lbl.grid(sticky=W, row = _row, column= 0, pady=4, padx=5)
        self.robot_list = Robots
        self.robot_ddm.set(self.robot_list[0])
        tk.OptionMenu(self, self.robot_ddm, *self.robot_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)

        _row = _row + 1

	    # Plan
        lbl = Label(self, text="Plan")
        lbl.grid(sticky=W, row = _row, column= 0, pady=4, padx=5)
        self.plan_list = Plans
        self.plan_ddm.set(self.plan_list[0])
        tk.OptionMenu(self, self.plan_ddm, *self.plan_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)

        # Buttons
        launchButton = Button(self, text="Exec",command=self.planexec_script)
        launchButton.grid(sticky=W, row=_row, column=2, pady=4, padx=5)        


        _row = _row + 1

        # Condition
        lbl = Label(self, text="Condition")
        lbl.grid(sticky=W, row=_row, column=0, pady=4, padx=5)
        self.cond_list = Conditions
        self.cond_ddm.set(self.cond_list[0])
        tk.OptionMenu(self, self.cond_ddm, *self.cond_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)


        # Buttons
        launchButton = Button(self, text="Send",command=self.condition_script)
        launchButton.grid(sticky=W, row=_row, column=2, pady=4, padx=5)        
    


    def planexec_script(self):
        print 'Robot %s Plan %s' % (self.robot_ddm.get(), self.plan_ddm.get())
        cmd = './runplan.sh %s %s' % (self.robot_ddm.get(), self.plan_ddm.get())
        print cmd
        os.system(cmd)

    def condition_script(self):
        print 'Robot %s Cond %s' % (self.robot_ddm.get(), self.cond_ddm.get())
        cmd = './pnpcond.sh %s %s' % (self.robot_ddm.get(), self.cond_ddm.get())
        print cmd
        os.system(cmd)

    


def main():
  
    root = tk.Tk()
    f = DIP(root)
    root.geometry("360x320+0+0")
    root.mainloop()
    f.quit()

  


if __name__ == '__main__':
    main()

