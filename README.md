# README #

ROS node, actions and fluents for exemplary HRI actions and fluents implementation.
Plans using these actions/fluents can be run with PNP.

To compile this node, just put (or link) it in the catkin_make/src folder and use 'catkin_make'.

## Run with docker ##

Requires: `docker` and `docker-compose` (install following [these instructions](https://docs.docker.com/compose/install/) )

Requires docker images:

    iocchi/stage_environments
    iocchi/rchomeedu-1804-melodic
    iocchi/pnp

Download docker images:

    docker pull iocchi/stage_environments
    docker pull iocchi/rchomeedu-1804-melodic
    docker pull iocchi/pnp

Quick start

    cd docker
    ./start.bash

Quick quit

    cd docker
    ./quit.bash


## Implemented actions and fluents



## Plans

To run a demo, use the 'demo_apps' scripts (see the README file there)

To generate a plan with PNPgen, use:


```
  $ ./genplan.sh <plan_file> <execution_rules>
```


To run a plan, use


```
  $ ./runplan.sh <plan_name> (without extension)

```


Example:


```
  $ ./genplan.sh peccioli_nav.plan peccioli_nav.er
  $ ./runplan.sh peccioli_nav

```


=============== 
AVAILABLE PLANS
===============

1. go_and_greet_person: simple demo for approaching and greeting a person

2. go_and_follow_person: simple demo for approaching, greeting and following a person

3. opendiag2016: interaction demo with several options

4. peccioli_nav: sample navigation in Peccioli@Home map
