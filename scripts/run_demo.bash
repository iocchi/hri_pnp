#!/bin/bash
#
# Use: ./run_demo.bash <plan_name>
#
# Plans: go_and_greet_person, go_and_follow_person, opendiag2016
#

PLAN=stop

if [ "$1" ]; then
PLAN=$1
fi

# Start simulated environment and robot

`(rospack find diago_apps)`/script/run_navigation.py diago DIS_first_floor SIM -initpose 12 2 270
# DISB1
#`(rospack find diago_apps)`/script/run_navigation.py diago diag_entrance SIM -initpose 12.5 19 270

sleep 5

xterm -e "roslaunch hri_pnp hri_pnpas.launch" &

sleep 5

echo Executing plan $PLAN

rostopic pub /diago/planToExec std_msgs/String "data: '$PLAN'"  --once

xterm -e "rostopic echo /diago/pnp/currentActivePlaces" &

sleep 5



xterm -e "./joyevents.py" &

#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'personAt_RoomB110'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'personAtPrinter'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'stopfollow'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'topic_hri'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'startinteraction'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'restart'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'followme'" --once


#cd ~/src/speech
#xterm -e "python simple_server.py" &
#cd -

#xterm -e "rosbag record -o $HOME/bags/makerfaire /diago/scan /diago/PNPConditionEvent /diago/pnp/currentActivePlaces /diago/PNP/goal /RCOMMessage; sleep 3" &






