#!/bin/bash
#
# Use: ./run_demo.bash <plan_name>
#
# Plans: go_and_greet_person, go_and_follow_person, opendiag2016
#

PLAN=stop

if [ "$1" ]; then
PLAN=$1
fi

# Start simulated environment and robot

`(rospack find diago_apps)`/script/run_navigation.py diago DIS_first_floor REAL -initpose 12 2 270

sleep 2

xterm -e "roslaunch hri_pnp hri_pnpas.launch" &

sleep 1

echo Executing plan $PLAN

rostopic pub /diago/planToExec std_msgs/String "data: '$PLAN'"  --once

xterm -e "rostopic echo /diago/pnp/currentActivePlaces" &

sleep 1

#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'personAt_RoomB110'" --once

xterm -e "./joyevents.py" &

#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'personAtPrinter'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'stopfollow'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'topic_hri'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'startinteraction'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'restart'" --once
#rostopic pub /diago/PNPConditionEvent std_msgs/String "data: 'followme'" --once


#cd ~/src/speech
#xterm -e "python simple_server.py" &
#cd -

xterm -e "rosbag record -o $HOME/bags/makerfaireR /diago/scan /diago/odom /diago/cmd_vel /diago/laser_obstacle_map /diago/PNPConditionEvent /diago/pnp/currentActivePlaces /diago/PNP/goal /RCOMMessage; sleep 3" &


