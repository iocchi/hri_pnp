#!/usr/bin/env python
# encoding: utf-8

# ROS imports
import roslib;
import rospy

# ROS msg and srv imports
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import String

# Python Libraries
import sys
import traceback

BTN_Blue=0
BTN_Green=1
BTN_Red=2
BTN_Orange=3

# command mapping
M = [ ['Blue','Cond','screentouched'],  
      ['Green','Plan','leon_TBM4'],
      ['Red','Cond','movefinished'], # ['Red','Plan','stop'],    
      ['Orange','Cond','MULTI']
     ]  



class JoyEvents(object):
    joyEnabled = True

    def __init__(self):
        # Initialize the Node
        rospy.init_node("joyevents")
        
        # Setup the Joy topic subscription
        self.joy_subscriber = rospy.Subscriber("/errazio/joy", Joy, self.handleJoyMessage, queue_size=1)
               
        self.PNP_event_pub =rospy.Publisher('/errazio/PNPConditionEvent', String, queue_size=1)
        
        self.PNP_plan_pub =rospy.Publisher('/errazio/planToExec', String, queue_size=1)
        
        print 'Joy events command mapping:'
        for i in range(4):
            print '   ',M[i][0],' -> ',M[i][1],' ',M[i][2]
        
        # Spin
        rospy.spin()
    
    
    def handleJoyMessage(self, data):
        """Handles incoming Joy messages"""
        
        if len(data.buttons) > 0:
          which_button=-1
          for i in range(4):
              if (data.buttons[i]==1):
                  which_button=i
          if (which_button>=0):
            print 'Joy event: ',M[which_button][0],' -> ',M[which_button][1],' ',M[which_button][2]
            
            if (M[which_button][1]=='Plan'):
                self.PNP_plan_pub.publish(M[which_button][2])
            elif (M[which_button][1]=='Cond'):
                if (M[which_button][2]=="MULTI"):
                    rate = rospy.Rate(5) # 5hz
                    self.PNP_event_pub.publish('start')
                    rate.sleep()
                    self.PNP_event_pub.publish('info')
                    rate.sleep()
                    self.PNP_event_pub.publish('HRIreceived')
                    rate.sleep()
                    self.PNP_event_pub.publish('answer_right')
                elif (which_button==2): # red
                    rate = rospy.Rate(5) # 5hz
                    self.PNP_event_pub.publish('screentouched')
                    rate.sleep()
                    self.PNP_event_pub.publish('HRIreceived')
                    rate.sleep()
                    self.PNP_event_pub.publish(M[which_button][2])
                    rate.sleep()
                    self.PNP_event_pub.publish('final')
                else:
                    self.PNP_event_pub.publish(M[which_button][2])
              
    

###  If Main  ###
if __name__ == '__main__':
    try:
        JoyEvents()
    except:
        rospy.logerr("Unhandled Exception in the joyevents Node:+\n"+traceback.format_exc())

